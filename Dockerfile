# Pin jupyterlab to Version 0.35.5
FROM jupyter/base-notebook:ff9357a77d78

ARG JUPYTERHUB_VERSION=0.9.*
RUN pip install --no-cache jupyterhub==$JUPYTERHUB_VERSION

# Install git so we can clone the nurtch demo notebook repo
USER root
RUN apt-get update && apt-get install -y git

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get -y install gcc mono-mcs musl-dev && \
    rm -rf /var/lib/apt/lists/*

# Switch back to notebook user (defined in the base image)
USER $NB_UID

# Install Rubix and other required packages on top of base Jupyter image
RUN pip install --no-cache \
    rubix \
    python-gitlab \
    scipy \
    numpy \
    pandas \
    scikit-learn \
    matplotlib \
    tensorflow \
    jupyterlab-git

# Install plotly extension for plotting metrics
RUN jupyter labextension install @jupyterlab/plotly-extension

# Install git extension to push/pull contents from GitLab repositories
RUN jupyter labextension install @jupyterlab/git
RUN jupyter serverextension enable --py jupyterlab_git
